import { Component } from '@angular/core';

@Component({
  selector: 'dshv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'volvo-front';
}
